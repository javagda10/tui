package com.sda;

import com.sda.options.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

public class TUI {

    private List<IOption> options = new ArrayList<>();

    public TUI() {
        this.options.add(new OptionAddStudent());           // 0
        this.options.add(new OptionSearchByIndex());        // 1
        this.options.add(new OptionSearchByGrades());       // 2
        this.options.add(new OptionListStudents());
    }

    public void printOptions() {
        System.out.println("1 - Dodaj studenta.");
        System.out.println("2 - Wyszukaj po indeksie.");
        System.out.println("3 - Wybierz z srednia nizsza niz.");
        System.out.println("4 - Wybierz studentów z roku.");
        System.out.println("5 - Wywal studenta.");
        System.out.println("6 - Quit.");
    }

    public void printOptionsList() {
        for (int i = 0; i < options.size(); i++) {
            System.out.println(i + ". " + options.get(i).printOption());
        }
    }

    public void start() {
        final Dziekanat dziekanat = new Dziekanat();

        boolean isWorking = true;
        while (isWorking) {
            printOptionsList();
            Optional<IOption> option = makeDecision();
            if (option.isPresent()) {
                option.get().enterOption(dziekanat);
            }
        }
    }

    private Optional<IOption> makeDecision() {
        Scanner scanner = new Scanner(System.in);
        String line = scanner.nextLine();

        try {
            int optionNumber = Integer.parseInt(line);

            return Optional.ofNullable(options.get(optionNumber));
        } catch (NumberFormatException nfe) {
            System.err.println("Wrong input!");
            return Optional.empty();
        } catch (IndexOutOfBoundsException aioobe) {
            System.err.println("Wrong option!");
            return Optional.empty();
        }
    }
}
