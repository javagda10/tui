package com.sda.options;

import com.sda.Dziekanat;
import com.sda.Student;

import java.util.List;
import java.util.Scanner;

public class OptionSearchByGrades implements IOption {
    @Override
    public String printOption() {
        return "Wybierz z srednia nizsza niz";
    }

    @Override
    public void enterOption(Dziekanat dziekanat) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Gimme grades: ");
        try {
            Double gradesMean = Double.parseDouble(scanner.nextLine());

            List<Student> studentList = dziekanat.searchByGrades(gradesMean);
            studentList.forEach(System.out::println);
        } catch (NumberFormatException nfe) {
            System.err.println("Error, wrong input.");
            return;
        }
    }
}
