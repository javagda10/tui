package com.sda.options;

import com.sda.Dziekanat;
import com.sda.Student;

import java.util.List;
import java.util.Optional;
import java.util.Scanner;

public class OptionSearchByIndex implements IOption {

    @Override
    public String printOption() {
        return "Wyszukaj po indeksie";
    }

    @Override
    public void enterOption(Dziekanat dziekanat) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Gimme index: ");
        String gradesMean = scanner.nextLine();

        Optional<Student> student = dziekanat.searchByIndex(gradesMean);
        System.out.println(student);
    }
}
