package com.sda.options;

import com.sda.Dziekanat;
import com.sda.Student;

import java.util.Scanner;

public class OptionAddStudent implements IOption {
    @Override
    public String printOption() {
        return "Dodaj studenta";
    }

    @Override
    public void enterOption(Dziekanat dziekanat) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Index:");
        String index = scanner.nextLine();
        System.out.println("Name:");
        String name = scanner.nextLine();
        System.out.println("Surname:");
        String surname = scanner.nextLine();

        try {
            System.out.println("Year:");
            String year = scanner.nextLine();
            int rok = Integer.parseInt(year);

            System.out.println("Grades:");
            String grades = scanner.nextLine();
            double gradesMean = Double.parseDouble(grades);

            Student student = new Student(index, name, surname, rok, gradesMean);
            dziekanat.addStudent(student);
        } catch (NumberFormatException nfe) {
            System.err.println("Wrong input!");
        }
    }
}
