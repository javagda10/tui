package com.sda.options;

import com.sda.Dziekanat;

public class OptionListStudents implements IOption {
    @Override
    public String printOption() {
        return "Wylistuj studentów";
    }

    @Override
    public void enterOption(Dziekanat dziekanat) {
        dziekanat.getStudentMap().values().forEach(System.out::println);
    }
}
