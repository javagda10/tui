package com.sda.options;

import com.sda.Dziekanat;

public interface IOption {
    public String printOption();

    public void enterOption(Dziekanat dziekanat);
}
