package com.sda;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class Dziekanat {

    private Map<String, Student> studentMap = new HashMap<>();

    public void addStudent(Student student) {
        studentMap.put(student.getIndex(), student);
    }

    public List<Student> searchByGrades(Double gradesMean) {
        return studentMap
                .values()
                .stream()
                .filter(student -> student.getGradesMean() < gradesMean)
                .collect(Collectors.toList());
    }

    public Optional<Student> searchByIndex(String index) {
        return Optional.ofNullable(studentMap.get(index));
    }

    public Map<String, Student> getStudentMap() {
        return studentMap;
    }
}
