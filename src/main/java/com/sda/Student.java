package com.sda;

public class Student {
    private String index, name, surname;
    private int year;
    private double gradesMean;

    public Student(String index, String name, String surname, int year, double gradesMean) {
        this.index = index;
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.gradesMean = gradesMean;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public double getGradesMean() {
        return gradesMean;
    }

    public void setGradesMean(double gradesMean) {
        this.gradesMean = gradesMean;
    }

    @Override
    public String toString() {
        return "Student{" +
                "index='" + index + '\'' +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", year=" + year +
                ", gradesMean=" + gradesMean +
                '}';
    }
}
